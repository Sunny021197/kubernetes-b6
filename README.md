# Kubernetes

## What is Kubernetes?
    Kubernetes is an open-source container orchestration platform developed by Google. It automates the deployment, scaling, and management of containerized applications. Containers are lightweight, standalone executable packages that include everything needed to run an application, including the code, runtime, system tools, and libraries. Kubernetes provides a framework for automating the deployment and management of these containers at scale.

    The main goal of Kubernetes is to simplify the management of containerized applications, allowing developers to focus on building and deploying their applications without worrying about the underlying infrastructure. It abstracts away the complexities of managing individual containers and provides a unified API and set of tools for managing and scaling containerized applications across a cluster of machines.
 - 

## Why we use kubernetes?
    kubernetes is a widely used container orchestration platform that provides a framework for automating the deployment, scaling, and management of containerized applications. Here are some reasons why Kubernetes is popular and widely used:

    Scalability: Kubernetes allows you to scale your applications easily. It automatically distributes containers across a cluster of machines, allowing you to handle increased traffic or workload by adding or removing containers as needed.

    High availability: Kubernetes ensures that your applications are highly available by automatically monitoring and replacing containers that fail or become unresponsive. It can also distribute your application across multiple nodes, reducing the risk of a single point of failure.

    Service discovery and load balancing: Kubernetes has built-in service discovery mechanisms that allow containers to find and communicate with each other. It also provides load balancing for distributing incoming network traffic across multiple containers to ensure efficient resource utilization.

    Self-healing: Kubernetes constantly monitors the health of containers and can automatically restart or replace failed containers. This ensures that your applications are resilient and always available.

    Portability: Kubernetes provides a consistent environment for deploying and running applications, regardless of the underlying infrastructure. It abstracts away the details of the infrastructure and allows you to deploy your applications on-premises, in the cloud, or across different cloud providers without modification.

    Declarative configuration and automation: Kubernetes allows you to define the desired state of your applications and infrastructure through declarative configuration files. It then automatically handles the deployment and management of resources to achieve that desired state. This simplifies the process of deploying and updating applications.

    Extensibility: Kubernetes is highly extensible and provides an extensive ecosystem of plugins, extensions, and third-party tools. You can integrate additional functionality such as monitoring, logging, and security into your Kubernetes clusters to meet your specific requirements.

    Community and ecosystem: Kubernetes has a large and active community of developers and contributors. This means there is a wealth of documentation, tutorials, and resources available to help you learn and troubleshoot any issues you may encounter. It also ensures that Kubernetes continues to evolve and improve over time.

    Overall, Kubernetes offers a powerful and flexible platform for managing containerized applications at scale, providing benefits such as scalability, high availability, automation, and portability, making it a popular choice for organizations adopting container-based architectures.

## Features of kubernetes?
    Key features of Kubernetes include:

    Container Orchestration: Kubernetes manages the scheduling and placement of containers across a cluster of machines, ensuring optimal resource utilization and high availability of applications.

    Automated Scaling: Kubernetes can automatically scale the number of containers based on application workload and resource demands, allowing applications to handle increased traffic and load.

    Service Discovery and Load Balancing: Kubernetes provides built-in mechanisms for service discovery and load balancing, allowing containers to communicate with each other and distribute traffic across multiple instances of an application.

    Rolling Updates and Rollbacks: Kubernetes supports rolling updates, allowing applications to be updated with minimal downtime by gradually replacing old containers with new ones. It also provides the ability to roll back to a previous version if any issues arise.

    Storage Orchestration: Kubernetes offers various options for managing and provisioning storage volumes for containers, including local storage, network-attached storage (NAS), and cloud storage.

    Self-Healing: Kubernetes continuously monitors the health of containers and automatically restarts or replaces failed containers to ensure the desired state of the application is maintained.

## Kubernetes Architecture?

    In Kubernetes, a Pod is the smallest and most basic unit of deployment. It represents a single instance of a running process within a cluster. The lifecycle of a Pod consists of several phases and transitions, which I'll explain below:

    Pending: When a Pod is created, it enters the Pending phase. In this phase, Kubernetes scheduler assigns the Pod to a node where it can run. The scheduler takes into account factors such as resource requirements, node affinity, and other scheduling constraints.

    Running: Once a Pod is assigned to a node, it transitions to the Running phase. In this phase, Kubernetes creates the necessary network and storage resources for the Pod. The containers within the Pod are started, and the Pod's status changes to "Running." At this point, the containers are executing the specified application or process.

    Succeeded/Failed: When the main process or application within the Pod completes its execution, the Pod enters the Succeeded or Failed phase, depending on the outcome. If the main process exits with a zero status code, indicating success, the Pod enters the Succeeded phase. Conversely, if the process exits with a non-zero status code, the Pod enters the Failed phase.

    Unknown: If the state of a Pod cannot be determined, typically due to an error or issue with the underlying infrastructure, the Pod enters the Unknown phase. This state is typically temporary and can be resolved once the underlying issue is fixed.

    Terminating: When a Pod is scheduled for deletion or when a higher-level controller (e.g., a Deployment or ReplicaSet) decides to scale down the number of replicas, the Pod enters the Terminating phase. In this phase, Kubernetes initiates the graceful termination process. The Pod's containers are sent termination signals, allowing them to clean up and exit gracefully. Once all containers have terminated, the Pod transitions to the Terminated phase.

    Terminated: The Terminated phase signifies that all containers within the Pod have successfully terminated. At this point, the Pod's resources are freed up, and it is no longer visible in the cluster.

    It's important to note that a Pod can transition between these phases due to various events, such as scaling, failures, or rescheduling. Kubernetes monitors the state of Pods and handles their lifecycle management to ensure the desired state is maintained based on the Pod specifications and cluster conditions.

## Pod Lifecycle

    In Kubernetes, a Pod is the smallest and most basic unit of deployment. It represents a single instance of a running process within a cluster. The lifecycle of a Pod consists of several phases and transitions, which I'll explain below:

    Pending: When a Pod is created, it enters the Pending phase. In this phase, Kubernetes scheduler assigns the Pod to a node where it can run. The scheduler takes into account factors such as resource requirements, node affinity, and other scheduling constraints.

    Running: Once a Pod is assigned to a node, it transitions to the Running phase. In this phase, Kubernetes creates the necessary network and storage resources for the Pod. The containers within the Pod are started, and the Pod's status changes to "Running." At this point, the containers are executing the specified application or process.

    Succeeded/Failed: When the main process or application within the Pod completes its execution, the Pod enters the Succeeded or Failed phase, depending on the outcome. If the main process exits with a zero status code, indicating success, the Pod enters the Succeeded phase. Conversely, if the process exits with a non-zero status code, the Pod enters the Failed phase.

    Unknown: If the state of a Pod cannot be determined, typically due to an error or issue with the underlying infrastructure, the Pod enters the Unknown phase. This state is typically temporary and can be resolved once the underlying issue is fixed.

    Terminating: When a Pod is scheduled for deletion or when a higher-level controller (e.g., a Deployment or ReplicaSet) decides to scale down the number of replicas, the Pod enters the Terminating phase. In this phase, Kubernetes initiates the graceful termination process. The Pod's containers are sent termination signals, allowing them to clean up and exit gracefully. Once all containers have terminated, the Pod transitions to the Terminated phase.

    Terminated: The Terminated phase signifies that all containers within the Pod have successfully terminated. At this point, the Pod's resources are freed up, and it is no longer visible in the cluster.

    It's important to note that a Pod can transition between these phases due to various events, such as scaling, failures, or rescheduling. Kubernetes monitors the state of Pods and handles their lifecycle management to ensure the desired state is maintained based on the Pod specifications and cluster conditions.

## Objects:
- Pod - Smallest unit of kubernetes. Its just wrapping around the container
- Service - Expose application 
    - ClusterIP - Expose application inside the cluster
    - NodePort - Expose application outside the cluster
    - LoadBalancer - Expose application outside the cluster 
- Namespace -  To segregate the resources / k8s objects